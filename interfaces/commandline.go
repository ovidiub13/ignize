package interfaces

import "gitlab.com/ovidiub13/ignize/entities"

// PictureInteractor is the entrypoint for interacting with Pictures
type PictureInteractor interface {
	GetPicture(location, name string) (entities.Picture, error)
	GetPictures(location string, maxCount int) ([]entities.Picture, error)
}

// CommandLineHandler in the user interface with the application via the CLI
type CommandLineHandler struct {
	PictureInteractor PictureInteractor
}

// ListPictures prints a max of `maxCount` Pictures to STDOUT
func (handler CommandLineHandler) ListPictures(location string, maxCount int) {

}
