package entities

import (
	"reflect"
)

// Picture represents a picture file
type Picture struct {
	Name     string
	Location string
}

// SameAs checks wether _picture_ is the same as _p_
func (p *Picture) SameAs(picture Picture) bool {
	if reflect.DeepEqual(*p, picture) {
		return true
	}
	return false
}
