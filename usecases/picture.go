package usecases

import (
	"fmt"

	"gitlab.com/ovidiub13/ignize/entities"
	"gitlab.com/ovidiub13/ignize/logger"
	"gitlab.com/ovidiub13/ignize/storage"
)

// PictureInteractor is the entrypoint for interacting with Pictures
type PictureInteractor struct {
	Storage storage.Storage
	Logger  logger.Logger
}

// GetPicture shows a Picture with it's tags
func (interactor *PictureInteractor) GetPicture(location, name string) (entities.Picture, error) {
	picture, err := interactor.Storage.GetPicture(location, name)
	if err != nil {
		// Picture does not exist
		message := fmt.Sprintf("The picture \"%s\" does not exist at location: \"%s\"", name, location)
		interactor.Logger.Log(message)
		return picture, err
	}
	return picture, nil
}

// GetPictures gets a maximum of `maxCount` `entities.Picture`s from `Storage`
func (interactor *PictureInteractor) GetPictures(location string, maxCount int) ([]entities.Picture, error) {
	pictures, err := interactor.Storage.GetPictures(location, maxCount)
	if err != nil {
		interactor.Logger.Log(err.Error())
		return pictures, err
	}
	return pictures, nil
}
